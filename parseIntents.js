const fs = require("fs");
const files = fs.readdirSync("./dialogflow");
const result = [];
files.forEach(path => {
    const name = path.replace(".json","");
    const arr = require(`./dialogflow/${path}`);
    const phrases = [];
    const resultObj = {name};
    arr.forEach(phraseObj => {
        phrases.push(phraseObj.data[0].text);
    });
    resultObj.phrases = phrases;
    result.push(resultObj);
});

module.exports = result;

/*
result = [{
    intentName1: name,
    phrases: [phrase1, phrase2, ...],
},{
    intentName2: name,
    phrases: [phrase1, phrase2, ...],
},
    ...
]
*/