const fs = require("fs");
const { NlpManager } = require("node-nlp");
const trainnlp = require("./train-nlp");

const nlpManager = new NlpManager({ languages: ["ru"] });

const review = fs.readFileSync("./review.csv", "utf8");

trainnlp(nlpManager, console.log).then(() => {
    return Promise.all(review.replace(/\(not set\)/g, "positive")
    .split("\n")
    .map(str => {
        const obj = {};
        let arr = str.split(",");
        obj.intent = arr.shift();
        obj.rating = arr.pop();
        obj.quantity = arr.pop();
        obj.query = arr.join(",");
        return obj;
    })
    .map(objDF => {
        return nlpManager.process(objDF.query).then(answer => {
            const objAXA = {};
            objAXA.query = answer.utterance;
            objAXA.intentAXA = answer.intent;
            objAXA.intentDF = objDF.intent;
            return objAXA;
        });
    }));
}).then(data => {
    fs.writeFileSync("./axa-result-all.csv", "IntentAXA, intentDF, Query\n" + data.map(obj => {
        return `${obj.intentAXA},${obj.intentDF},${obj.query}`;
    }).join("\n"));
    fs.writeFileSync("./axa-result-diff.csv", "IntentAXA, intentDF, Query\n" + data.filter(obj => {
        return  !(obj.intentAXA === obj.intentDF || (obj.intentAXA === "None" && obj.intentDF === "input.unknown"));
    }).map(obj => {
        return `${obj.intentAXA},${obj.intentDF},${obj.query}`;
    }).join("\n"));
});
